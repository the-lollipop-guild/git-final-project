import numpy as np
import itertools


class BestPairsFinder:

    def create_even_number_of_particles(self, num_particles, dimension, seed=17657):
        if num_particles % 2 != 0:
            raise ValueError('num_particles must be even')

        np.random.seed(seed)
        particles = np.empty((num_particles,dimension))
        for i in range(num_particles):
            for j in range(dimension):
                particles[i][j] = np.random.random()
        return particles.tolist()
        pass

    def find_distance(self, particle_positions):
        """
        Find distance between two particles
        """
        N_particles=len(particle_positions)
        dims=[len(np.array([particle_positions[i]])) for i in range(N_particles)]
        assert all(dim == dims[0] for dim in dims)

        dist = np.array([N_particles])
        for i in range(int(N_particles-1)):
            dist=np.sqrt(np.sum((np.array([particle_positions[i]])-np.array([particle_positions[i+1]]))**2))
        print(dist)
        return dist
        pass

    def all_pairs_distance(self, particle_positions):
        """
        Finds distance between all particle pairs. The input is a list of coordinates (particles) in 1D, 2D, or 3D.
        The number particles must be even. The output is an upper triangular matrix which contains the distances
        between each pair of particles.
        """
        N=int(len(particle_positions))
        dims=[len(np.array([particle_positions[i]])) for i in range(N)]
        assert all(dim == dims[0] for dim in dims)

        dist = np.zeros((N,N))
        for n in range(int(N-1)):
            for i in range(n+1,N):
                dist[n][i] = np.sqrt(np.sum((np.array([particle_positions[n]])-np.array([particle_positions[i]]))**2))
        return dist
        pass

    def calc_shortest_pair_list(self, pair_lists, dist_mat):
        shortest_list = None
        shortest_path = None
        for list in pair_lists:
            sum = 0
            for pair in list:
                if pair[0] < pair[1]:
                    sum = sum + dist_mat[pair[0]][pair[1]]
                else:
                    sum = sum + dist_mat[pair[1]][pair[0]]
            if shortest_path == None or sum < shortest_path:
                shortest_path = sum
                shortest_list = list
        return shortest_path, shortest_list
        pass

    @staticmethod
    def find_best_pairs_1d(particle_positions):
        """
        Given a list of particles in 1-dimension, groups particle indexes into the "best" pairs.

        Args:
            particle_positions list(list): list of 1-dimensional particle positions


        Returns:
            pairs list(list(tuple)): list of list (to be compatible with calc_shortest_pair_list()) of tuples of indexes
            corresponding to the "best" pairs.
        """

        dims = [len(particle_positions[i]) for i in range(len(particle_positions))]
        assert all(dim == 1 for dim in dims)

        particle_list = [i[0] for i in particle_positions]
        sorted_indexes = np.argsort(particle_list)

        pairs = [(sorted_indexes[i], sorted_indexes[i+1]) for i in range(0, len(sorted_indexes), 2)]

        return [pairs]

    @staticmethod
    def find_pair_combinations(particle_positions):
        """
        Given a list of particles, groups particle indexes into unique pair combinations. Returns a list of list of
        tuples suitable for calc_shortest_pair_list().

        Args:
            particle_positions list(list): list of 1-dimensional particle positions

        Returns:
            pairs list(list(tuple)): list of list (to be compatible with calc_shortest_pair_list()) of tuples of indexes
            corresponding to the "best" pairs.
        """
        full_list = []
        dummy_indices = itertools.product(*[range(i) for i in reversed(range(1, len(particle_positions), 2))])
        list_double_index = [*dummy_indices]
        for double_index in list_double_index:
            index_list = [*range(len(particle_positions))]
            pair_list = []
            for index in double_index:
                zeroth = index_list.pop(0)
                ith = index_list.pop(index)
                pair_list.append((zeroth, ith))
            full_list.append(pair_list)

        return full_list

    def find_best_pairs(self, particle_positions):
        """
        This method integrates all the other methods/functions of the class so that the best pairs can be found
        for arbitrary numbers of particles in 1, 2, or 3 dimensions.

        """
        distance_matrix = self.all_pairs_distance(particles)

        dims = [len(particle_positions[i]) for i in range(len(particle_positions))]
        if all(d == 1 for d in dims):
            best_distance, best_pair_list = self.calc_shortest_pair_list(self.find_best_pairs_1d(particles),
                                                                         distance_matrix)
            return best_distance, best_pair_list

        else:
            all_pair_lists = BP.find_pair_combinations(particle_positions)
            best_distance, best_pair_list = self.calc_shortest_pair_list(all_pair_lists,
                                                                         distance_matrix)
            return best_distance, best_pair_list


if __name__ == "__main__":
    BP = BestPairsFinder()

    print("4 PARTICLES")
    print("\n1D, from given problem example")
    particles = [[-10], [-1], [-2], [1]]
    print("All possible pairs\n", BP.find_pair_combinations(particles))
    result, best_pairs = BP.find_best_pairs(particles)
    print("Best distance of example problem: " + str(result))
    print("Best pairs list of example problem: " + str(best_pairs))

    print("##############################################################")
    print("\nusing particle generator (1D)")
    n_particles = 4
    dim = 1
    particles = BP.create_even_number_of_particles(n_particles, dim)
    print(particles)
    print("All possible pairs\n", BP.find_pair_combinations(particles))
    result, best_pairs = BP.find_best_pairs(particles)
    print("Best distance of generated 1D particles: " + str(result))
    print("Best pairs list of example problem (using find_best_pairs_1d): " + str(best_pairs))

    print("##############################################################")
    print("\n2D (extended example)")
    particles = [[-10, -10], [-1, -1], [-2, -2], [1, 1]]
    print("All possible pairs\n", BP.find_pair_combinations(particles))
    result, best_pairs = BP.find_best_pairs(particles)
    print("Best distance of 2D extended example problem: " + str(result))
    print("Best pairs list of 2D extended example problem: " + str(best_pairs))

    print("##############################################################")
    print("\nusing particle generator (2D)")
    n_particles = 4
    dim = 2
    particles = BP.create_even_number_of_particles(n_particles, dim)
    print(particles)
    print("All possible pairs\n", BP.find_pair_combinations(particles))
    result, best_pairs = BP.find_best_pairs(particles)
    print("Best distance of 2D generated particles: " + str(result))
    print("Best pairs list of 2D generated particles: " + str(best_pairs))

    print("##############################################################")
    print("\n3D (extended example)")
    particles = [[-10, -10, -10], [-1, -1, -1], [-2, -2, -2], [1, 1, 1]]
    print("All possible pairs\n", BP.find_pair_combinations(particles))
    result, best_pairs = BP.find_best_pairs(particles)
    print("Best distance of 3D extended example problem: " + str(result))
    print("Best pairs list of 3D extended example problem: " + str(best_pairs))

    print("##############################################################")
    print("\nusing particle generator (3D)")
    n_particles = 4
    dim = 3
    particles = BP.create_even_number_of_particles(n_particles, dim)
    print(particles)
    print("All possible pairs\n", BP.find_pair_combinations(particles))
    result, best_pairs = BP.find_best_pairs(particles)
    print("Best distance of 3D generated particles: " + str(result))
    print("Best pairs list of 3D generated particles: " + str(best_pairs))

    print("##############################################################")
    print("##############################################################")
    print("\n\n6 PARTICLES")
    print("1D example + 2 nearby particles")
    particles = [[-10], [-1], [-2], [1], [2], [3]]
    print("All possible pairs\n", BP.find_pair_combinations(particles))
    result, best_pairs = BP.find_best_pairs(particles)
    print("Best distance of 6-particle example: " + str(result))
    print("Best pairs list of 6-particle example: " + str(best_pairs))

    print("##############################################################")
    print("\n1D generated 6 particles")
    particles = BP.create_even_number_of_particles(6, 1)
    print(particles)
    print("All possible pairs\n", BP.find_pair_combinations(particles))
    result, best_pairs = BP.find_best_pairs(particles)
    print("Best distance of generated 6-particle problem: " + str(result))
    print("Best pairs list of generated 6-particle problem: " + str(best_pairs))

    print("##############################################################")
    print("##############################################################")
    import time
    n_particles = int(input("\n\nPlease select a number of particles [4 by default]: ") or "4")
    dim = int(input("Please select number of dimensions [1 by default]: ") or "1")
    particles = BP.create_even_number_of_particles(n_particles, dim)
    before = time.perf_counter()
    result, best_pairs = BP.find_best_pairs(particles)
    after = time.perf_counter()
    print("elapsed time:", after - before, "seconds")
    print("Best distance of selected {}-particle {}-D problem: ".format(n_particles, dim) + str(result))
    print("Best pairs list of selected {}-particle {}-D problem: ".format(n_particles, dim) + str(best_pairs))

    # 4 particles in 2D: 0.0003687919999997291 seconds
    # 4 particles in 3D: 0.0003704999999998293 seconds

    # 6 particles in 2D: 0.00081395800000017 seconds
    # 6 particles in 3D: 0.0007233749999997485 seconds

    # 8 particles in 2D: 0.002602667000000558 seconds
    # 8 particles in 3D: 0.00263420800000036 seconds

    # 10 particles in 2D: 0.007843916999999756 seconds
    # 10 particles in 3D: 0.007834667000000017 seconds

    # 12 particles in 2D: 0.052477833000000196 seconds
    # 12 particles in 3D: 0.05562108300000013 seconds

    # 14 particles in 2D: 0.7558800419999994 seconds
    # 14 particles in 3D: 0.7562409589999999 seconds

    # 16 particles in 2D: 14.504227458999999 seconds
    # 16 particles in 3D: 14.628656541000002 seconds
