# Best Pairs Finder

Given an even number of particles, this module is able 
to find groups of the closest possible pairs. The module
relies on a number of functions in order to find these
groups. The use and execution of each of these functions
is described in detail in individual sections.

## Main function

### Create_even_number_of_particles function
This function randomly generates coordinates for required number of particles in specified dimensional space.
A fixed seed is initialized in the function which generates the paticle coordinates consistently even when repeated multiple times. 

#### Input
The input for this function
are the number of particles and dimensions of the coordinates to be generated.
The number of particles specified must be even or else you will receive an error.
Sample coordinates generated for two particles in two dimension would look as follows:
[[0.769749131946113, 0.9228646209386234], [0.451167888968656, 0.19329838709658942]].

#### Execution
Sample execution code for generating list of particles using ```create_even_number_of_particles()```
is: 

```python
from src.best_pairs_finder import BestPairsFinder
BP = BestPairsFinder
num_particles = 2
dim_particles = 2
particles = BP.create_even_number_of_particles(num_particles, dim_particles)
```

#### Output
The output of this function is particles with randomly generated coordinates.


### Distance matrix function
The distance function is used to find the distances
between all particle pairs. It returns an upper triangular
matrix of the distances between pairs of particles.

#### Input
The input for this function
is a list of coordinates (particles) in 1D, 2D, or 3D.
The number of particles must be even and must all have
the same dimension or else you will receive an error.
A sample list of four particles in 2D would look as follows:
[[(0, 0), (1, 1)], [(20, 20), (21, 21)]].

#### Execution
Sample execution code for the list of particles above
is: 
```
pairs_finder.find_best_pairs(particle_positions=[[(0, 0), (1, 1)], [(20, 20), (21, 21)]])
```

#### Output
The output of this function is an upper triangular matrix
which contains the distances between each particle pair.

### Find best pairs in 1D function

#### Input
The input argument for ```find_best_pairs_1d()``` is also list of 
particle coordinates, but restricted to particles in 1 dimension.  

#### Execution
Once the best pairs finder class is instantiated, ```find_best_pairs_1d()```
can be called as a method of this class:

```python
from src.best_pairs_finder import BestPairsFinder
BP = BestPairsFinder()
particles = [[-10.], [-1.], [-2.], [1.]]
pair_list = BP.find_best_pairs_1d(particles)
```

#### Output
The list of lists of tuples returned (```pair_list``` in the execution example)
contains the combination of pairs that will minimize the sum over distances.
```pair_list``` can then be passed to ```calc_shortest_pair_list()``` method to evaluate the 
minimized sum over distances. 


### Function to find all possible pair combinations

#### Input
The input argument for ```find_pair_combinations()``` is a list of 
particle coordinates, can be particles in 1, 2, or 3 dimensions.  

#### Execution
Once the best pairs finder class is instantiated, ```find_pair_combinations()```
can be called as a method of this class:

```python
from src.best_pairs_finder import BestPairsFinder
BP = BestPairsFinder()
particles = [[-10.], [-1.], [-2.], [1.]]
pair_lists = BP.find_pair_combinations(particles)
```

#### Output
The list of lists of tuples returned (```pair_lists``` in the execution example)
contains the all possible unique combinations of pairs of particles.
```pair_lists``` can then be passed to ```calc_shortest_pair_list()``` method to evaluate the 
pair that minimizes sum over distances, and the minimized sum over distances. 

### Find best pairs function
This class method integrates all the other methods to solve the problem for a 
given list of particles.

#### Input
The input argument for ```find_best_pairs()``` is a list of 
particle coordinates, can be particles in 1, 2, or 3 dimensions.  

#### Execution
Once the best pairs finder class is instantiated, ```find_pair_combinations()```
can be called as a method of this class:

```python
from src.best_pairs_finder import BestPairsFinder
BP = BestPairsFinder()
particles = [[-10.], [-1.], [-2.], [1.]]
best_distance, best_pairs = BP.find_best_pairs(particles)
```

#### Output
A tuple of the ```(best_distance, best_pairs)``` is returned to the user. 
```best_distance``` is the minimized sum over particle distances, ```best_pairs``` is the 
list of particle pairs that minimizes the sum over particle distances.