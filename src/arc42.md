# Best Pairs Finder

## Goal
The purpose of this module is to, given an even number of 
particles, find the way to group these particles in 
pairs in such a way that the target function:
$\sum_{pairs<i,j>}f(r_i,r_j)$
is minimized. We consider $f$ to be the distance 
between the two particles. Our module is designed with the intention that the
function can be minimized for a system of 100 particles
in less than 30 seconds on one CPU.

## Constraints
We are constrained to the timeframe of the Munchkin course
in order to do this task (i.e. Feb. and March 2022).

## Context & Scope

Rather than to come up with new methodologies or sophisticated approaches
to solve this problem, we would like to come up with a solution using the
practices such as test driven development and agile project management
discussed in the Munchkin lectures. Therefore, our work here does not pioneer
new methods for solving this problem, but instead highlights the fundamentals
of test driven development and agile project management in its execution.

## Solution Strategy

Our solution relies on test driven development strategies; therefore, every
aspect of our module has a corresponding test. We also used agile project
management for our solution, which is why our module is divided into four
sub-functions, each of which serve an independent purpose as well as working
together to minimize the target function. Therefore, this module can both
be used for its intended purpose, to minimize the given function, or the
sub-functions can be used independently for their own individual purposes.

## Building Block View
The module relies on four primary functions:
- Particle generator function
- Distance matrix function
- Pairs generator function
- Minimization function

In tandem, these functions minimize the target function. A full description 
of each of these sub-functions can be found in the ```src/index.md``` file of the
module. This file also includes sample inputs and outputs for each of the
sub-functions.