from unittest import TestCase, skip

import numpy as np

from src.best_pairs_finder import BestPairsFinder



class TestBestPairsFinder(TestCase):

    def test_nothing(self):
        self.assertTrue(True)

    def test_genparticles(self):
        BP = BestPairsFinder
        num_particles = 4
        dim_particles = 4
        particles = BP.create_even_number_of_particles(self, num_particles, dim_particles)
        self.assertTrue(True)

    def test_1D_zero(self):
        BP=BestPairsFinder
        self.assertEqual(0,BP.find_distance(self,[0,0]))  # add assertion here

    def test_1D_two(self):
        BP=BestPairsFinder
        self.assertEqual(2,BP.find_distance(self,[2,0]))  # add assertion here

    def test_2D_zero(self):
        BP=BestPairsFinder
        self.assertEqual(0,BP.find_distance(self,[[0,0],[0,0]]))  # add assertion here

    def test_2D_dist(self):
        BP=BestPairsFinder
        num_particles = 2
        dim_particles = 2
        particles = BP.create_even_number_of_particles(self, num_particles, dim_particles)
        dist = BP.find_distance(self,particles)
        self.assertTrue(True)

    def test_int_matrix(self):
        BP=BestPairsFinder
        num_particles = 2
        dim_particles = 2
        particles = BP.create_even_number_of_particles(self, num_particles, dim_particles)
        dist = BP.all_pairs_distance(self,particles)
        self.assertTrue(True)

    @skip
    def test_find_best_pairs_zero_particles(self):
        pairs_finder = BestPairsFinder()
        particle_positions = list()
        best_pairs = pairs_finder.find_best_pairs(particle_positions=particle_positions)
        self.assertTrue((()) == best_pairs)

    @skip
    def test_find_best_pairs_two_particles_in_one_dimensions(self):
        pairs_finder = BestPairsFinder()
        particle_positions = [(0., ), (2., )]
        best_pairs = pairs_finder.find_best_pairs(particle_positions=particle_positions)
        for pairs in best_pairs:
            pairs.sort()
        self.assertCountEqual([[(0., ), (2., )]], best_pairs)

    @skip
    def test_find_best_pairs_four_particles_in_one_dimensions(self):
        pairs_finder = BestPairsFinder()
        particle_positions = [(21, ), (1, ), (0, ), (21, )]
        best_pairs = pairs_finder.find_best_pairs(particle_positions=particle_positions)
        for pairs in best_pairs:
            pairs.sort()
        self.assertCountEqual([[(0, ), (1, )], [(20, ), (21, )]], best_pairs)

    @skip
    def test_find_best_pairs_two_particles_in_two_dimensions(self):
        pairs_finder = BestPairsFinder()
        particle_positions = [(0., -1.), (1., 2.)]
        best_pairs = pairs_finder.find_best_pairs(particle_positions=particle_positions)
        for pairs in best_pairs:
            pairs.sort()
        self.assertCountEqual([[(0., -1.), (1., 2.)]], best_pairs)

    @skip
    def test_find_best_pairs_four_particles_in_two_dimensions(self):
        pairs_finder = BestPairsFinder()
        particle_positions = [(0, 0), (1, 1), (20, 20), (21, 21)]
        best_pairs = pairs_finder.find_best_pairs(particle_positions=particle_positions)
        for pairs in best_pairs:
            pairs.sort()
        self.assertCountEqual([[(0, 0), (1, 1)], [(20, 20), (21, 21)]], best_pairs)

class TestAllPairsDist(TestCase):

    def test_nothing(self):
        BP=BestPairsFinder
        particle_positions = list()
        pair_dist = BP.all_pairs_distance(self,particle_positions=particle_positions)
        self.assertCountEqual([],pair_dist.tolist())  # add assertion here

    def test_1D_zero(self):
        BP=BestPairsFinder
        particle_positions = [(0., ), (0., )]
        pair_dist = BP.all_pairs_distance(self,particle_positions=particle_positions)
        self.assertCountEqual([[0,0],[0,0]],pair_dist.tolist())

    def test_1D_nonzero(self):
        BP=BestPairsFinder
        particle_positions = [(0., ), (2., )]
        pair_dist = BP.all_pairs_distance(self,particle_positions=particle_positions)
        self.assertCountEqual([[0,2],[0,0]],pair_dist.tolist())

    def test_2D_nonzero(self):
        BP=BestPairsFinder
        particle_positions = [(0., -1.), (1., 2.)]
        pair_dist = BP.all_pairs_distance(self,particle_positions=particle_positions)
        self.assertCountEqual([[0,np.sqrt(10)],[0,0]],pair_dist.tolist())

    def test_1D_4parts(self):
        BP=BestPairsFinder
        particle_positions = [(-10,), (-1.,), (-2.,), (1.,)]
        pair_dist = BP.all_pairs_distance(self,particle_positions=particle_positions)
        self.assertCountEqual([[0,9,8,11],[0,0,1,2],[0,0,0,3],[0,0,0,0]],pair_dist.tolist())

    def test_2D_4parts(self):
        BP=BestPairsFinder
        particle_positions = [(-10,2), (-1.,3), (-2.,5), (1.,8)]
        pair_dist = BP.all_pairs_distance(self,particle_positions=particle_positions)
        self.assertCountEqual([[0,np.sqrt(82),np.sqrt(73),np.sqrt(157)],[0,0,np.sqrt(5),np.sqrt(29)],[0,0,0,np.sqrt(18)],[0,0,0,0]],pair_dist.tolist())


class TestShortestPairList(TestCase):

    def test_nothing(self):
        BP = BestPairsFinder
        particle_positions = list()
        pair_dist = BP.all_pairs_distance(self, particle_positions=particle_positions)
        self.assertCountEqual([], pair_dist.tolist())

    def test_1D_zeros(self):
        BP = BestPairsFinder
        particle_positions = [(0.,), (0.,)]
        pair_list1 = [[(0,1)]]
        pair_list2 = [[(1,0)]]
        pair_dist = BP.all_pairs_distance(self, particle_positions=particle_positions)
        dist, list = BP.calc_shortest_pair_list(self, pair_list1, pair_dist)
        self.assertEqual(0, dist)
        self.assertEqual([(0,1)], list)
        dist, list = BP.calc_shortest_pair_list(self, pair_list2, pair_dist)
        self.assertEqual(0, dist)
        self.assertEqual([(1,0)],list)

    def test_1D_notzeros(self):
        BP = BestPairsFinder
        particle_positions = [(0.,), (2.,)]
        pair_list1 = [[(0,1)]]
        pair_list2 = [[(1,0)]]
        pair_dist = BP.all_pairs_distance(self, particle_positions=particle_positions)
        result, list = BP.calc_shortest_pair_list(self, pair_list1, pair_dist)
        self.assertEqual(2, result)
        result, list = BP.calc_shortest_pair_list(self, pair_list2, pair_dist)
        self.assertEqual(2, result)

    def test_2D_notzeros(self):
        BP = BestPairsFinder
        particle_positions = [(0., -1.), (1., 2.)]
        pair_list1 = [[(0,1)]]
        pair_list2 = [[(1,0)]]
        pair_dist = BP.all_pairs_distance(self, particle_positions=particle_positions)
        result, list = BP.calc_shortest_pair_list(self, pair_list1, pair_dist)
        self.assertEqual(np.sqrt(10), result)
        result, list = BP.calc_shortest_pair_list(self, pair_list2, pair_dist)
        self.assertEqual(np.sqrt(10), result)

    def test_1D_4particles(self):
        BP = BestPairsFinder
        particle_positions = [(-10,), (-1.,), (-2.,), (1.,)]
        pair_list1 = [[(0,1),(2,3)],[(0,2),(1,3)],[(0,3),(1,2)]]
        pair_list2 = [[(1,0),(3,2)],[(2,0),(3,1)],[(3,0),(2,1)]]
        pair_dist = BP.all_pairs_distance(self, particle_positions=particle_positions)
        result, list = BP.calc_shortest_pair_list(self, pair_list1, pair_dist)
        self.assertEqual(10, result)
        result, list = BP.calc_shortest_pair_list(self, pair_list2, pair_dist)
        self.assertEqual(10, result)

    def test_2D_4particles(self):
        BP = BestPairsFinder
        particle_positions = [(-10,2), (-1.,3), (-2.,5), (1.,8)]
        pair_list1 = [[(0,1),(2,3)],[(0,2),(1,3)],[(0,3),(1,2)]]
        pair_list2 = [[(1,0),(3,2)],[(2,0),(3,1)],[(3,0),(2,1)]]
        pair_dist = BP.all_pairs_distance(self, particle_positions=particle_positions)
        result, list = BP.calc_shortest_pair_list(self, pair_list1, pair_dist)
        self.assertEqual(np.sqrt(82)+np.sqrt(18), result)
        result, list = BP.calc_shortest_pair_list(self, pair_list2, pair_dist)
        self.assertEqual(np.sqrt(82)+np.sqrt(18), result)


class TestFindCombinations1D(TestCase):

    def test_nothing(self):
        BP = BestPairsFinder
        particle_positions = list()
        best_pairs = BP.find_best_pairs_1d(particle_positions)
        self.assertCountEqual([[]], best_pairs)

    def test_1D_zeros(self):
        BP = BestPairsFinder
        particle_positions = [(0.,), (0.,)]
        best_pairs = BP.find_best_pairs_1d(particle_positions)
        self.assertEqual([[(0, 1)]], best_pairs)

    def test_1D_notzeros(self):
        BP = BestPairsFinder
        particle_positions = [(0.,), (2.,)]
        best_pairs = BP.find_best_pairs_1d(particle_positions)
        self.assertEqual([[(0, 1)]], best_pairs)

    def test_1D_example(self):
        """ 4 particle example given in problem statement. """
        BP = BestPairsFinder
        particle_positions = [(-10.,), (-1.,), (-2.,), (1.,)]
        best_pairs = BP.find_best_pairs_1d(particle_positions)
        self.assertEqual([[(0, 2), (1, 3)]], best_pairs)


class TestFindPairCombinations(TestCase):
    def test_nothing(self):
        BP = BestPairsFinder
        particle_positions = list()
        pairs = BP.find_pair_combinations(particle_positions)
        self.assertCountEqual([[]], pairs)

    def test_1D_zeros(self):
        BP = BestPairsFinder
        particle_positions = [(0.,), (0.,)]
        best_pairs = BP.find_pair_combinations(particle_positions)
        self.assertEqual([[(0, 1)]], best_pairs)

    def test_2D_zeros(self):
        BP = BestPairsFinder
        particle_positions = [(0., 0.,), (0., 0.,)]
        best_pairs = BP.find_pair_combinations(particle_positions)
        self.assertEqual([[(0, 1)]], best_pairs)

    def test_3D_zeros(self):
        BP = BestPairsFinder
        particle_positions = [(0., 0., 0.,), (0., 0., 0.,)]
        best_pairs = BP.find_pair_combinations(particle_positions)
        self.assertEqual([[(0, 1)]], best_pairs)

    def test_1D_notzeros(self):
        BP = BestPairsFinder
        particle_positions = [(0.,), (2.,)]
        pair_lists = BP.find_pair_combinations(particle_positions)
        pair_dist = BP.all_pairs_distance(self, particle_positions=particle_positions)
        result, _ = BP.calc_shortest_pair_list(self, pair_lists, pair_dist)
        self.assertEqual(2, result)

        particle_positions = [(2.,), (0.,)]
        pair_lists = BP.find_pair_combinations(particle_positions)
        pair_dist = BP.all_pairs_distance(self, particle_positions=particle_positions)
        result, _ = BP.calc_shortest_pair_list(self, pair_lists, pair_dist)
        self.assertEqual(2, result)

    def test_2D_notzeros(self):
        BP = BestPairsFinder
        particle_positions = [(0., -1.), (1., 2.)]
        pair_lists = BP.find_pair_combinations(particle_positions)
        pair_dist = BP.all_pairs_distance(self, particle_positions=particle_positions)
        result, _ = BP.calc_shortest_pair_list(self, pair_lists, pair_dist)
        self.assertEqual(np.sqrt(10), result)

        particle_positions = [(1., 2.), (0., -1.)]
        pair_lists = BP.find_pair_combinations(particle_positions)
        pair_dist = BP.all_pairs_distance(self, particle_positions=particle_positions)
        result, _ = BP.calc_shortest_pair_list(self, pair_lists, pair_dist)
        self.assertEqual(np.sqrt(10), result)

    def test_1D_example(self):
        """ 4 particle example given in problem statement. This checks if all possible pair combinations are found."""
        BP = BestPairsFinder
        particle_positions = [(-10.,), (-1.,), (-2.,), (1.,)]
        pairs = BP.find_pair_combinations(particle_positions)
        self.assertEqual([[(0, 1), (2, 3)], [(0, 2), (1, 3)], [(0, 3), (1, 2)]], pairs)

    def test_1D_6particles(self):
        """ 6 particles test with known answer. This checks if best pairs are found for 6 particles
        once all possible pair combinations are found."""
        BP = BestPairsFinder
        particle_positions = [(-10.,), (-1.,), (-2.,), (1.,), (2.,), (3.,)]
        pairs = BP.find_pair_combinations(particle_positions)
        pair_dist = BP.all_pairs_distance(self, particle_positions=particle_positions)
        _, best_pair_list = BP.calc_shortest_pair_list(self, pairs, pair_dist)
        self.assertEqual([(0, 2), (1, 3), (4, 5)], best_pair_list)
