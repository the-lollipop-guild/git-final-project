# The Best Pairs Finder Project

Welcome to the Best Pairs Finder module.

## How to install the module

Our git repository is at has the following HTTPS address:
https://gitlab.com/the-lollipop-guild/git-final-project.git.
Once you have cloned this repository, you should install the required 
python packages contained in the file ```requirements.txt```.

## Primary module functions

The bulk of the module can be found in ```best_pairs_finder.py```. This
file contains four sub-functions in addition to the main module function.

## Tests

Both unit tests for all of our individual sub-functions as well as integration
tests can be found in ```src/test_best_pairs_finder.py```.

## Further Documentation

Basic user documentation can be found in the file ```src/index.md```.
This file also includes basic input and output examples for the module.
Further developer documentation can be found in ```src/arc42.md```.